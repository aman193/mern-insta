const express = require('express')
const PORT = 5000
const {MONGOURL} = require("./keys")
const mongoose = require('mongoose')
const cors = require('cors')
const app = express()


mongoose.connect(MONGOURL, {
    useNewUrlParser:true,
    useUnifiedTopology:true
})

mongoose.connection.on('connected', () => {
    console.log('Connected to mongoDb...')
})

mongoose.connection.on('error', (err) => {
    console.log("Error Connecting ", err)
})

require('./models/user')
require('./models/post')
app.use(express.json())
app.use(cors())
app.use(require('./routes/auth'))
app.use(require('./routes/post'))


app.listen(PORT, () => {
    console.log("server is running on " + PORT)
})