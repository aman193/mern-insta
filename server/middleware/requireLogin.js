const jwt = require('jsonwebtoken')
const {JWT} = require('../keys')
const mongoose = require('mongoose')
const User = mongoose.model('User')

module.exports = (req, res, next) => {
    const {authorization} = req.headers
    if(!authorization){
        return res.status(404).json({error:"You Must Be Logged in"})
    }
    const token = authorization.replace("Bearer ", "")
    jwt.verify(token, JWT,(err, payload) => {
        if(err){
            return res.status(401).json({error:"You Must Be Logged in"})
        }
        const {_id} = payload
        User.findById(_id)
        .then(userData=>{
            req.user = userData
            next()
        })
    })
}