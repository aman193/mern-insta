const express = require("express")
const router = express.Router()
const mongoose = require('mongoose')
const User = mongoose.model("User")
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {JWT} = require('../keys')
const requireLogin = require('../middleware/requireLogin')

router.post('/signup', (req, res) => {
    const {name, email, password} = req.body
    if(!email || !password || !name)
    {
        return res.status(422).json({error:"Plz add all the fields"})
    }
    User.findOne({email})
    .then((savedUser) => {
        if(savedUser){
            return res.status(422).json({error:"User already exists!"})
        }
        bcrypt.hash(password, 12)
        .then(hashedPassword => {
            const user = new User({
                name, 
                email,
                password:hashedPassword
            }) 
    
            user.save()
            .then(user => {
                res.json({message:"Sign Up successfully!!!"})
            })
            .catch(err => {
                console.log(err)
            })
        })
    })
    .catch(err => {
        console.log(err)
    })
})

router.post('/signin', (req, res) => {
    const {email, password} = req.body
    if(!email || !password){
        return res.status(422).json("Plz add email or password")
    }
    User.findOne({email})
    .then(savedUser => {
        if(!savedUser){
            return res.status(422).json("Invalid email or Password")
        }
        bcrypt.compare(password, savedUser.password)
        .then(doMatch=>{
            if(doMatch){
                const token = jwt.sign({_id:savedUser._id}, JWT)
                const {_id, name, email} = savedUser
                res.json({token, user:{_id, name, email}})
            }
            else{
                return res.status(422).json("Invalid email or Password")
            }
        })
        .catch(err => {
            console.log(err)
        })
    })
})



module.exports = router